<?php


/**
 * @file
 * Functions supporting bookimport.module.
 *
 * Copyright (C) 2006 Djun M. Kim
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * See the GNU General Public License version 2 LICENSE file for
 * full terms and conditions of use.
 *
 */

// global hash provides mapping from old nid to new
$new_nid = array();
$parent_of = array();

/**
 * Fills in the body of the given node.
 * Note: @arg $parent is not actually used
 */
function save($tnode, $nid, $parent, $mode) {
    global $new_nid;
    global $parent_of;

    if (DEBUG > 2) { 
        echo "saving node '". $tnode->title . "' ";
        echo "(id = ". $tnode->id .", mode = $mode)\n";

        // set up metadata
        $nodeid = preg_replace('@^(node-)(\d+)$@', '\2', $tnode->id);
        $nidinfo  = "  nid:$nodeid\n";
        $nidinfo .= "  title:$tnode->title\n";
        $nidinfo .= "  md5:". $tnode->get_md5_body() ."\n";
        $nidinfo .= "  weight:". $tnode->get_weight() ."\n";
        $nidinfo .= "  depth:". $tnode->get_depth() ."\n";
        $nidinfo .= "  parent:". $tnode->get_parent() ."\n";
        
        echo "<pre style=\"font-family: courier; font-size: 8pt;\">";
        echo "$nidinfo</pre>"; 
    }

    if ($mode == 'update') {
        _update_book_node($tnode);
    }
    else if ($mode == 'insert') {
        _create_book_node($tnode);
    }
    else {
        // Write node body
    }
}

/**
 * Creates a new node in the database
 */
function _create_book_node($tnode) {
    global $user;
    global $new_nid;
    global $parent_of;

    $node->type = $tnode->nodetype;
    $node->title = $tnode->title;
    $node->uid = $tnode->uid;
    $node->status = $tnode->status;
    $node->created = $tnode->created;
    $node->promote = $tnode->promote;
    $node->moderate = $tnode->moderatep;
    $node->changed = $tnode->changed;
    $node->sticky = $tnode->sticky;

    $node->format = $tnode->format;
    $node->weight = $tnode->weight;
    $node->body = $tnode->body;
    $node->author = $tnode->author;

    // $node->uid = $user->uid;
    // $node->create = time();

    node_save($node);
    if (DEBUG > 1)  { 
        echo "new nid is: ". $node->nid ."<br />";
    }
    // identify newly created nodes by mapping new_id => new_id;
    // newly created nodes are ones which have an empty $tnode->id
    if ($tnode->id == '') {
        $tnode->id = $node->nid;
    }
    // get the new nid, store it hashed by old nid:
    $new_nid[$tnode->id] = $node->nid;
    // record the parent-child of the node, 
    // based on the existing ids
    $parent_of[$tnode->id] = $tnode->parent;

    if (DEBUG > 1)  {
        echo "created: nid = ". $node->nid ."\n"; 
    };
    return ($node->nid);
}

function _update_book_node($tnode) {
    global $parent_of;

    $timestamp = time();
    $log = "\nInserted on ". date("D F j, Y") ." at ". date("G:i:s T") ."<br />\n";

    $node->type = $tnode->nodetype;
    $node->title = $tnode->title;
    $node->uid = $tnode->uid;
    $node->status = $tnode->status;
    $node->created = $tnode->created;
    $node->promote = $tnode->promote;
    $node->moderate = $tnode->moderatep;
    $node->changed = $tnode->changed;
    $node->sticky = $tnode->sticky;

    $node->format = $tnode->format;
    $node->weight = $tnode->weight;
    $node->body = $tnode->body;
    $node->author = $tnode->author;

    $node->nid = $tnode->id;

    /* this saves the node; creates a new revision, etc. */

    node_save($node);
    if (DEBUG > 1)  { 
        echo "new nid is: ". $node->nid ."<br />";
    }

    // Since we're updating, we don't need to remap nids
    // record the parent-child of the node, 
    // based on the existing ids
    $parent_of[$node->nid] = $tnode->parent;

    return ($node->nid);
}


/**
 * Updates a given node in the database
 */
function update($tnode) {
    // We need to deal with case where tnode does not have an id.
    // In this case, create the node rather than updating
    // an existing node.

    // echo "update: tnode->id = " . $tnode->id. "<br />\n"; 

    if (!isset($tnode->id) || $tnode->id == 0) {
        _create_book_node($tnode);
    }
    else { // were updating an existing entry
        _update_book_node($tnode);
    }
}
    
# at end, if inserting new records iterate over new_nid
function make_links() {
    global $new_nid;
    global $parent_of;


    if (DEBUG > 0) {
        echo "make_links()<br />"; 
    };
    if (DEBUG > 2) { 
        echo "<pre>\n"; 
        print "'Parent of' array:";
        print_r($parent_of);
        print "Old nid => New nid:";
        print_r($new_nid);
        echo "</pre>\n"; 
    };

    /* The book table entry will have been created by 
       the 'node_save()' operation since the type
       of the node is 'book'.  We just need to update
       the table to get the parents and such right */
    $book_sql = 
        db_rewrite_sql(
                       "UPDATE {book} ".
                       "SET ".
                       "  nid=%d, ".
                       "  parent=%d, ".
                       "  weight=%d ".
                       "WHERE".
                       "  vid='%d' "
                       );
    
    if (!empty($new_nid)) {
        foreach ($new_nid as $nid => $new_value) {
            $node = node_load(array('nid' => $new_value));
            $node->nid = $new_value;
            $node->parent = $new_nid[$parent_of[$nid]]; 
            
            /*
             $log = "Imported on " . date("D F j, Y") . " at " . date("G:i:s T") . "<br />";
             $log .= "parent[$nid ($new_value)] = " .$parent_of[$nid]. " ($this_parent)";
             $log .= $node->log;
            */
            if (!isset($nid) || $nid == '') {
                $node->parent = $parent_of[$nid];
                $nid = $new_value;
            }
            
            if (DEBUG > 0) { 
                echo sprintf("UPDATE {book} ".
                             "SET ".
                             "  nid=%d, ".
                             "  parent=%d, ".
                             "  weight=%d ".
                             "WHERE".
                             "  vid=%d ",
                             $node->nid,
                             $node->parent,
                             $node->weight,
                             $node->vid
                             );
                echo "<br />";
            }    
            $result = db_query($book_sql,
                               $node->nid,
                               $node->parent,
                               $node->weight,
                               $node->vid
                               );
            
            if (DEBUG > 0) { 
              echo "parent[$nid ($new_value)] = ". $parent_of[$nid] ." ($node->parent)<br />"; 
            }
        }
    }
}

?>
