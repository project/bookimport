<?php


/**
 * @file
 * Defines the node class, used by export and import modules
 *
 * Copyright (C) 2006 Djun M. Kim
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * See the GNU General Public License version 2 LICENSE file for
 * full terms and conditions of use.
 *
 */


/**
 * This class represents a Drupal node.

% This is the node table:
| nid      | int(10) unsigned |      | PRI | NULL    | auto_increment |
| type     | varchar(32)      |      | MUL |         |                |
| title    | varchar(128)     |      | MUL |         |                |
| uid      | int(10)          |      | MUL | 0       |                |
| status   | int(4)           |      | MUL | 1       |                |
| created  | int(11)          |      | MUL | 0       |                |
| comment  | int(2)           |      |     | 0       |                |
| promote  | int(2)           |      | MUL | 0       |                |
| moderate | int(2)           |      | MUL | 0       |                |
| changed  | int(11)          |      | MUL | 0       |                |
| sticky   | int(2)           |      |     | 0       |                |
| vid      | int(10) unsigned |      |     | 0       |                |

% This is the node_revisions table:
| nid       | int(10) unsigned |      | MUL | 0       |       |
| vid       | int(10) unsigned |      | PRI | 0       |       |
| uid       | int(10)          |      | MUL | 0       |       |
| title     | varchar(128)     |      |     |         |       |
| body      | longtext         | YES  |     | NULL    |       |
| teaser    | longtext         | YES  |     | NULL    |       |
| timestamp | int(11)          |      |     | 0       |       |
| format    | int(4)           |      |     | 0       |       |
| log       | longtext         | YES  |     | NULL    |       |


 */
class node {
    /* these are fields in the node table */
    var $id;
    var $nodetype;
    var $uid;
    var $status;
    var $created;
    var $comment;
    var $promote;
    var $moderate;
    var $changed;
    var $sticky;
    var $vid;

    /* these are fields in the book table */
    var $parent;
    var $weight;

    /* these are fields in the revision table */
    var $title;
    var $body;
    var $teaser;
    var $format;
    var $log;

    /* these are derived or computed */
    var $md5_body;
    var $author;
    var $depth;


    function set_id($id) { $this->id = $id; }
    function get_id() { return $this->id; }
    function set_nodetype($nodetype) { $this->nodetype = $nodetype; }
    function get_nodetype() { return $this->nodetype; }
    function set_uid($uid) { $this->uid = $uid; }
    function get_uid() {return $this->uid; }
    function set_status($status) { $this->status = $status; }
    function get_status() {return $this->status; }
    function set_created($created) { $this->created = $created; }
    function get_created() {return $this->created; }

    function set_promote($promote) { $this->promote = $promote; }
    function get_promote() {return $this->promote; }
    function set_moderate($moderate) { $this->moderate = $moderate; }
    function get_moderate() {return $this->moderate; }
    function set_changed($changed) { $this->changed = $changed; }
    function get_changed() {return $this->changed; }
    function set_sticky($sticky) { $this->sticky = $sticky; }
    function get_sticky() {return $this->sticky; }
    function set_vid($vid) { $this->vid = $vid; }
    function get_vid() { return $this->vid; }

    function set_weight($weight) { $this->weight = $weight; }
    function get_weight() { return $this->weight; }
    function set_parent($parent) { $this->parent = $parent; }
    function get_parent() { return $this->parent; }

    function set_title($title) { $this->title = $title; }
    function get_title() { return $this->title; }
    function set_body($body) { $this->body = $body; }
    function get_body() { return $this->body; }
    function set_teaser($teaser) { $this->teaser = $teaser; }
    function get_teaser() { return $this->teaser; }
    function set_format($format) { $this->format = $format; }
    function get_format() {return $this->format; }

    function set_md5_body($md5_body) { $this->md5_body = $md5_body; }
    function get_md5_body() { return $this->md5_body; }
    function set_author($author) { $this->author = $author; }
    function get_author() {return $this->author; }
    function set_depth($depth) { $this->depth = $depth; }
    function get_depth() { return $this->depth; }

}
?>
