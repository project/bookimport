#!/usr/bin/perl -w

use File::Find;
use HTML::Tidy;

$dir = $ARGV[0];
print "start dir: $dir\n";

$tidy = new HTML::Tidy;
$tidy -> ignore( type => TIDY_WARNING );

sub process_file() {
    $name = $File::Find::name ;
    print "name = $name\n";
    open(FILE, $name) or die "Can't open file $name: $!\n";

    @contents = <FILE>;
    close(FILE);

    print "before:\n";
    print "contents:\n@contents\nend contents\n";

    print "after:\n";
    if ($tidy->parse($name, $contents)) {
	if ($tidy->clean(@contents)) {
	    print @contents;
	}
	else {
	    print "problem: ";
	}
	print @contents;
    }	
    else {
	print "Error: ";
	for my $message ( $tidy->messages() ) {
	    print $message;
	}
	$tidy->clear_messages();
    }
}

find (\&process_file, @ARGV);

exit 0;

opendir(DIR, $dir) or die "Can't opendir $dir: $!\n";
while (defined($file = readdir(DIR))) {
    next if $file =~ /^\.\.?$/;  # skip . and ..
    print "file is $file\n";
}
closedir(DIR);
