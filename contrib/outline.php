#!/usr/bin/php
<?php

require_once('node.php');
require_once('save_node.php');
require_once('drupal_export_parser.php');

define('DEBUG', 0);

$file = $_SERVER['argv'][1];

$xml = xml_parser_create();
$obj = new drupal_export_parser('start_node', 'end_node'); // a class to assist with parsing

xml_set_object($xml, $obj);
xml_set_element_handler($xml, 'start_element', 'end_element');
xml_set_character_data_handler($xml, 'character_data');
xml_parser_set_option($xml, XML_OPTION_CASE_FOLDING, false);

# Open the XML file and parse it
$fp = fopen($file, 'r') or die("Can't read XML data $file.");
while ($data = fread($fp, 4096)) {
    xml_parse($xml, $data, feof($fp)) 
        or die("Can't parse XML data\n");
}
fclose($fp);

xml_parser_free($xml);

$indent = 0;
$current_tag = '';

$chapter = 0;
$section = 0;
$subsection = 0;
$subsubsection = 0;

foreach ($nodes as $node) {
    switch ($node->depth) {
    case 2: 
        $chapter++; 
        $section = 0;
        $subsection = 0;
        $subsubsection = 0;
        break;
    case 3: 
        $section++; 
        $subsection = 0;
        $subsubsection = 0;
        break;
    case 4: 
        $subsection++; 
        $subsubsection = 0;
        break;
    case 5: 
        $subsubsection++; 
        break;
    }

    echo "$node->depth: ";
    for ($i = 1; $i < $node->depth; $i++) { echo "  "; }
    if ($node->depth < 6) {
        if ($chapter) {printf("%2.d.", $chapter); }
        if ($section) {printf("%d.", $section); }
        if ($subsection) {printf("%d.", $subsection); }
        if ($subsubsection) {printf("%d.", $subsubsection); }
        echo " ";
    }
    else {
        print("         ");
    }
    echo "$node->title\n";
}

$nodes = array();

function start_node($tnode, $new_nid, $parent_of, $op) {
    global $nodes;

    if (DEBUG > 0) { 
        echo "start_node($tnode->id, $new_nid, $parent_of, $op)\n";
        echo "new_nid '" . $new_nid . "' \n";
        echo "node '" . $tnode->title . "' \n";
        echo "(id = " . $tnode->id .")\n";
        print_r($tnode);
    }
    $nodes[$new_nid] = $tnode;
}

function end_node($tnode, $new_nid, $parent_of, $op) {
    global $nodes;

    if (DEBUG > 0) { 
        echo "end_node($tnode->id, $new_nid, $parent_of, $op)\n";
        echo "node '" . $tnode->title . "' \n";
        echo "(id = " . $tnode->id .")\n";
        echo "==============================\ntnode = ";
        print_r($tnode);
    }
    $nodes[$tnode->id] = $tnode;
}



?>
