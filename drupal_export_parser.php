<?php
/** 
 * This is a helper class for PHP's XML parser, which implements
 * behaviour for the parser, namely how it show handle start
 * tags, end tags, and character data.
 *
 */
class drupal_export_parser {
    
    var $node = array();
    var $current_tag = '';
    var $current_title = '';
    var $current_body = '';
    var $current_teaser = '';
    var $current_parent = 0;
    var $current_nodeid = 0;
    var $action;
    var $phpimport;

    var $end_elt_callback;
    var $start_elt_callback;

    /** 
     *
     */
    function drupal_export_parser($start_callback = '', $end_callback = '') {
        global $end_elt_callback;
        global $start_elt_callback;
        global $node;

        $node = array();        
        $this->end_elt_callback = $end_callback;
        $this->start_elt_callback = $start_callback;

        $end_elt_callback = $end_callback;
        $start_elt_callback = $start_callback;

        if (DEBUG > 1) {
            echo "constructor for class drupal_export_parser\n";
            echo "setting end_callback to '$end_elt_callback'\n";
            echo "setting start_callback to '$start_elt_callback'\n";
        }
    }

    /** 
     * This function is called each time the XML parser encounters
     * an opening tag.
     */
    function start_element($parser, $tag, $attributes) {
        global $current_tag;
        global $node;
        global $current_body;
        global $current_teaser;
        global $current_parent; 
        global $current_nodeid; 
        global $start_elt_callback;
        
        if (DEBUG > 0) { echo "start_element($tag)...\n"; 
          if (count($attributes) > 0) {
            print_r($attributes);
          }
        }

        if ('book' == $tag) {
            $current_body = '';
            $current_teaser = '';
            $current_title = '';
        }
        if ('node' == $tag) {
            // prepare to create node; save node id attribute;
            // save content (body) in a node
            
            $tnode = new node();
            $nodeid = preg_replace('@^(node-)(\d+)$@', '\2', $attributes['id']);
            $tnode->set_id($nodeid);
            $tnode->set_body($current_body);
            $current_body = '';

            $current_nodeid = $tnode->id;
            $tnode->set_parent($current_parent);
            
            if (DEBUG > 2) {
                echo "current node id: $current_nodeid\n";
                echo "current parent id:$current_parent\n";
                echo "parent of $current_nodeid is $current_parent\n";
            }
            
            if (DEBUG > 1) { echo "creating node $current_nodeid\n"; }
            // push the node
            // echo "<b>push</b> node .. count before = ". count($node) ."\n";
            array_push($node, $tnode);
            // echo "count after: ". count($node) . "\n";

            //echo "node is: ";
            //print_r($node);

            $current_parent = $current_nodeid;

            if (function_exists($start_elt_callback)) {
                // echo "calling callback $start_elt_callback\n";
                call_user_func($start_elt_callback, $tnode, $tnode->id, $current_parent, $this->action, $this->phpimport);
            }
            else {
                // echo "can't call callback: $start_elt_callback!\n";
            }

        }
        if ('title' == $tag) {
            $current_title = '';
        }
        if ('nodeinfo' == $tag) {
            // echo "nodeinfo: pop node ..$node \n";
            // echo "<b>pop</b> node .. count before = ". count($node) ."\n";
            $tnode =  array_pop($node);
            // echo "count after: ". count($node) . "\n";

            $tnode->set_depth($attributes['depth']);
            $tnode->set_weight($attributes['weight']);
            $tnode->set_md5_body($attributes['md5_body']);
            $tnode->set_nodetype($attributes['type']);
            $tnode->set_author($attributes['author']);
            $tnode->set_uid($attributes['uid']);
            $tnode->set_created($attributes['created']);
            $tnode->set_status($attributes['status']);
            $tnode->set_format($attributes['format']);
            $tnode->set_sticky($attributes['sticky']);
            $tnode->set_promote($attributes['promote']);

            // echo "<b>push</b> node .. count before = ". count($node) ."\n";
            array_push($node, $tnode);
            // echo "count after: ". count($node) . "\n";

            $current_body = '';
        }
        if ('content' == $tag) {
            $current_body = '';
        }
        if ('teaser' == $tag) {
            $current_teaser = '';
        }
        $current_tag = $tag;
    }
    
    /** 
     * This function is called each time the XML parser encounters
     * a closing tag.
     */
    function end_element($parser, $tag) {
        global $current_title;
        global $current_body;
        global $current_teaser;
        global $current_tag;
        global $current_parent;
        global $end_elt_callback;
        global $node;
        
        if (DEBUG > 0) { echo "end_element($current_tag)...\n"; }
        
        if ('book' == $tag) {
            $current_body = '';
            $current_title = '';
        }
        if ('node' == $tag) {
            // echo "<b>pop</b> node .. count before = ". count($node) ."\n";
            $tnode = array_pop($node);
            // echo "count after: ". count($node) . "\n";

            if (function_exists($end_elt_callback)) {
                // echo "calling callback $end_elt_callback\n";
                call_user_func($end_elt_callback, $tnode, $tnode->id, $tnode->parent,  $this->action, $this->phpimport);
            }
            else {
                // echo "can't call callback: $end_elt_callback!\n";
            }
            
            // Done with this node 
            
            // reset to start new body;
            $current_body = '';
            $current_teaser = '';
            $current_tag = '';
            $current_parent = $tnode->get_parent();
        }
        if ('title' == $tag) {
            // Set the title of tnode
            // echo "end_element: pop node ..\n";
            // print_r($node);

            // echo "<b>pop</b> node .. count before = ". count($node) ."\n";
            $tnode = array_pop($node);
            // echo "count after: ". count($node) . "\n";
            
            // echo "end_element: tnode = \n";
            // print_r($node);


            $tnode->set_title($current_title);
            
            // push the node
            // echo "<b>push</b> node .. count before = ". count($node) ."\n";
            array_push($node, $tnode);
            // echo "count after: ". count($node) . "\n";

            $current_tag = '';
            $current_title = '';
        }
        if ('content' == $tag) {
            // echo "<b>pop</b> node .. count before = ". count($node) ."\n";
            $tnode = array_pop($node);
            // echo "count after: ". count($node) . "\n";

            $tnode -> set_body($current_body);
            
            // push the node
            // echo "<b>push</b> node .. count before = ". count($node) ."\n";
            array_push($node, $tnode);
            // echo "count after: ". count($node) . "\n";
            
            // start new body;
            $current_body = '';
            $current_teaser = '';
            $current_tag = '';
        }
        if ('teaser' == $tag) {
            // echo "<b>pop</b> node .. count before = ". count($node) ."\n";
            $tnode = array_pop($node);
            // echo "count after: ". count($node) . "\n";

            $tnode -> set_teaser($current_teaser);
            
            // push the node
            // echo "<b>push</b> node .. count before = ". count($node) ."\n";
            array_push($node, $tnode);
            // echo "count after: ". count($node) . "\n";
            
            // start new teaser;
            $current_teaser = '';
            $current_tag = '';
        }
        if ('nodeinfo' == $tag) {
            // echo "done nodeinfo: $current_body\n";


            // start new body;
            $current_body = '';
            $current_teaser = '';
            $current_tag = '';
        }
    }
    
    /** 
     * This function is called each time the XML parser encounters
     * character data (e.g., the contents of a tag)
     */
    function character_data($parser, $data) {
        global $current_title;
        global $current_body;
        global $current_teaser;
        global $current_tag;
        global $node;
        
        if (DEBUG > 3) { echo "character_data($current_tag"; };
        if (DEBUG > 4) { echo ", $data"; };
        if (DEBUG > 3) { echo ")\n"; };
        
        if ('title' == $current_tag) {
            $current_title .= $data;
            // echo "char data:current_title = $current_title;<br/>";
        }
        else if ('content' == $current_tag) {
            $current_body .= $data;
        }
        else if ('teaser' == $current_tag) {
            $current_teaser .= $data;
        }
        else if ('nodeinfo' == $current_tag) {
            // echo "chardata: nodeinfo: $data";
            $current_body .= $data;
        }
        else {
            // print "cdata:";
        }
    }
}
?>
