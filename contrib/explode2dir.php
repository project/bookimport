#!/usr/bin/php
<?php

include_once('node.php');
include_once('drupal_export_parser.php');

define('DEBUG', 0);

$file = $argv[1];

// Create the XML parser
$xml = xml_parser_create();

// Create the helper class (event handlers)
$obj = new drupal_export_parser('start_nodedir', 'write_snippet'); 

xml_set_object($xml, $obj);
xml_set_element_handler($xml, 'start_element', 'end_element');
xml_set_character_data_handler($xml, 'character_data');
xml_parser_set_option($xml, XML_OPTION_CASE_FOLDING, false);

// Open the XML file and parse it
$fp = fopen($file, 'r') or die("Can't read XML file $file: ");
while ($data = fread($fp, 4096)) {
    xml_parse($xml, $data, feof($fp)) 
        or die("Can't parse XML data\n");
}
fclose($fp);

// Clean up
xml_parser_free($xml);

/**
 * create dir, change to dir
 */
function start_nodedir($tnode, $nid, $parent, $op) {

    // create the directory associated with a node and change 
    // into the directory

    if (DEBUG > 2) { 
        echo "*************** start_nodedir()\n";
    }

    $filename = sprintf("%s", $tnode->id);
    mkdir($filename);
    
    if (DEBUG > 2) { echo "cd $filename\n"; }
    chdir($filename);
    
}

/**
 * Writes the body of the given node to a file in the current
 * directory.  Writes a file named 'nid' containing node id,
 * node class and title metadata into the current directory.
 */
function write_snippet($tnode, $nid, $parent, $op) {
    
    if (DEBUG > 0) { 
        echo "writing file '" . $tnode->title . "' ";
        echo "(id=$tnode->id)\n";
    }
    
    // Write node body
    $filename = '-' . $tnode->title;
    $filename = preg_replace('@/@', '&#38;', $filename);
    
    $fp1 = fopen($filename, 'w') 
        or die("Can't open file $filename for writing.");
    if (fwrite($fp1, $tnode->body) === FALSE) {
        die("Cannot write to file ($filename)\n");
    }
    fclose($fp1);
    
    // write metadata
    $nodeid = preg_replace('@^(node-)(\d+)$@', '\2', $tnode->id);
    $nidinfo = '';
    $nidinfo .= "nid:$nodeid\n";
    $nidinfo .= "title:$tnode->title\n";
    $nidinfo .= "md5:".$tnode->get_md5_body()."\n";
    $nidinfo .= "weight:".$tnode->get_weight()."\n";
    $nidinfo .= "depth:".$tnode->get_depth()."\n";
    //$nidinfo .= "class:$tnode->class\n";
    
    $filename = "nid";
    $fp2 = fopen($filename, 'w') 
        or die("Can't open file $filename for writing.");
    if (fwrite($fp2, $nidinfo) === FALSE) {
        die("Cannot write to file ($filename)\n");
    }
    fclose($fp2);


    if (DEBUG > 1) { echo "cd ..\n"; }
    chdir('..');

    // rename the directory with the title (rather than node id)    
    $filename = sprintf("[%02.2d]-%s-%s", $tnode->weight, $tnode->title, $tnode->id);
    $filename = preg_replace('@/@', '&#38;', $filename);
    $oldfilename = sprintf("%s", $tnode->id);

    if (DEBUG > 1) { echo "renaming directory to $filename from $oldfilename\n"; }
    
    if (!rename($oldfilename, $filename)) {
        echo "Error: can't rename $oldfilename to $filename\n";
    }

}

?>
