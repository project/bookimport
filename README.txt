Bookimport module
=================

Description:
------------
        
This module allows import of Drupal books, from exported Drupal book
XML files.  Being able to export books in this way permits
bulk/offline editing, import of books from external content, and
moving of books between Drupal sites. 

Please be aware that this is somewhat new code - make sure that you
*back up your Drupal database* prior to using this module to import
books.

I'm interested in hearing about your experiences with this software.
Please report issues via the Drupal issue tracker: 

http://drupal.org/project/issues

or contact me at the address given at the end of this file.


'Contributed' files
-------------------

These are extra little bits of code which may be useful.

explode2dir is a command-line PHP script which takes a
dxml file and creates a directory hierarchy containing
the content of that file.  Use as follows:

   % explode2dir mybook.xml

where mybook.xml is an xml file obtained from 'export_dxml', for
example.  

Note: explode2dir expects that every node in the file has a unique
nid.  This will be the case with files created by exporting from
Drupal using export_dxml.module.

explode2dir expects to find node.php and drupal_export_parser.php
in the working directory (usually the directory that explode2dir
is in)

If you use this as a Unix shell script, you many need to change
the first line from '#!/usr/bin/php' to reflect the path to
your command line PHP interpreter.

outline.php is a command line PHP script that reads a DXML file
and produces an indented table of contents style listing of the
contents.

tidy-traverse.pl is a perl script that recursively descends
through a directory and applies 'tidy', the W3C HTML validator
and corrector, to each plain file it finds.


Credits
-------

The first version of Bookimport module was based in part on the
Import/Export module of Tomas Mandys (mostly framework/menu/forms code
at this point)

History
-------

2005-07-15: First (alpha) release.
2005-08-04: Beta release
2005-09-01: Released as Drupal.org module
2006-01-25: Updated to Drupal 4.7 API

Author
-------

Djun Kim (puregin [at] puregin.org)

